<?php 
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'r00t');
define('DB_DATABASE', 'c5-bs3-html5bp');
define('PASSWORD_SALT', 'gzqA1HJNrCZrO772qyFNJGytDYPvuwAZSAUecAaKhnFWIjSXnISlBVkQBVyD6FG3');

/* 
## http://www.weblicating.com/c5/site-php/
## Set advanced permissions on
define('PERMISSIONS_MODEL', 'advanced');
 
## Use APC Caching (you need APC installed)
define('CACHE_LIBRARY', 'apc');
 
## Disable Zend Cache Cleaning (may improve performance)
define('CACHE_FRONTEND_OPTIONS',serialize(array('automatic_cleaning_factor' => 0)));
 
## Set time to 24 hour format
define('DATE_FORM_HELPER_FORMAT_HOUR', '24');
 
## Date Formatting
define('DATE_APP_GENERIC_MDYT_FULL', 'F d, Y \a\t g:i A');
define('DATE_APP_GENERIC_MDYT', 'n/j/Y \a\t g:i A');
define('DATE_APP_GENERIC_MDY', 'n/j/Y');
define('DATE_APP_GENERIC_MDY_FULL', 'F d, Y');
 
## Change minimum user name length from default value 3
define('USER_USERNAME_MINIMUM', 3);
 
## Change maximum username length from default value 64
define('USER_USERNAME_MAXIMUM', 64);
 
## Change minimum password length from default value 3
define('USER_PASSWORD_MINIMUM', 5);
 
## Change maximum password length from default value 64
define('USER_PASSWORD_MAXIMUM', 64);
 
## Change session time from default of 2 hours
define('SESSION_MAX_LIFETIME', 7200); // 2 hours
 
## Set registration email notification address
define('EMAIL_ADDRESS_REGISTER_NOTIFICATION','example@domain.com');
 
## Set registration email notification from address
define('EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM','example@domain.com');
 
 
## White Label Configuration
 
## Disable concrete5 marketplace integration.
define('ENABLE_MARKETPLACE_SUPPORT', false); 
 
## Disable help searches in the intelligent search.
define('ENABLE_INTELLIGENT_SEARCH_HELP', false);
 
## Disable marketplace add-ons appear in intelligent search results. 
define('ENABLE_INTELLIGENT_SEARCH_MARKETPLACE', false); 
 
## Disable newsflow
define('ENABLE_NEWSFLOW_OVERLAY', false);
 
## Path to the logo image. This can also be a full URL. This file should be 49x49 pixels.
define('WHITE_LABEL_LOGO_SRC', 'PATH TO FILE'); 
 
## Alt text for the logo
define('WHITE_LABEL_APP_NAME', 'TEXT');
 
## Disable Layouts.
define('ENABLE_AREA_LAYOUTS', false); 
 
## Disable Custom Design for blocks or areas.
define('ENABLE_CUSTOM_DESIGN', false);
 
## Disable Newsflow connect to concrete5.org to retrieve latest updates. 
define('ENABLE_APP_NEWS', false); 
 
## Set to a valid image (either local or a remote URL), or none.
define('WHITE_LABEL_DASHBOARD_BACKGROUND_SRC', 'none');
 
## Web address for today's Image
define('WHITE_LABEL_DASHBOARD_BACKGROUND_FEED', false); */