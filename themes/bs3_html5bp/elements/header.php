<?php   defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="sl">
	
<head>
	<meta charset="utf-8" />
	<?php  Loader::element('header_required'); ?>

	<!-- CSS: implied media=all -->
	<link rel="stylesheet" href="<?php echo $this->getThemePath()?>/css/bootstrap-test.css" /> 
	<link rel="stylesheet" href="<?php echo $this->getThemePath()?>/css/font-awesome.css" />
	<link rel="stylesheet" href="<?php echo $this->getThemePath()?>/css/superfish.css" />
	<link rel="stylesheet" href="<?php echo $this->getThemePath()?>/style.css" />
	<!-- <link rel="stylesheet" href="<?php echo $this->getThemePath()?>/typography.css" /> -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css' />
	<!--[if IE 7 ]>
	<link rel="stylesheet" href="<?php echo $this->getThemePath()?>/css/font-awesome-ie7.min.css" />
	<![endif]-->
	<!-- end CSS-->

	<!-- Mobile viewport -->
	<meta name="viewport" content="width=device-width" />

	<!-- favicon.ico and apple-touch-icon.png -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png"/>
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

	<!-- google analytics :: asynchronius script so it doesn't stop body content from loading :: don't hurting page speed :: replace XXXXXX and uncomment the script -->
	<!--
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-XXXXXXXXX']);
		_gaq.push(['_trackPageview']);
		
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	-->
</head>

<body>
	<div class="c5w container bss">
		<header class="row">
			<div id="header-area" class="span16">
				<div id="topMenu">
					<div class="floatLeft"><i class="icon-phone-sign icon-large swRed" title="Pokličite nas."></i> 041 768 455</div>
					<div class="floatLeft"><i class="icon-envelope-alt icon-large swRed" title="Pišite nam."></i> <a href="mailto:info.www@siware.net">info.www@siware.net</a></div>
					<div class="floatRight">
						<a href="/siware/" title="Na začetno stran"><i class="icon-home icon-large swRed"></i></a>&nbsp;&nbsp;&nbsp;
						<a href="/siware/kontakt/" title="Kontakt"><i class="icon-envelope-alt icon-large swRed"></i></a>&nbsp;&nbsp;&nbsp;
						<a href="/siware/sitemap/" title="Kazalo strani"><i class="icon-sitemap icon-large swRed"></i></a>
					</div>
				</div>
				<div class="logo">
					<a href="/siware/"><img src="<?php echo $this->getThemePath()?>/img/siware-main-logo.png" alt="na Si.Ware prvo stran" title="na Si.Ware prvo stran" /></a>
				</div>
				<nav id="mainMenu">
				    <a class="icon" href="/"><img src="<?php echo $this->getThemePath()?>/img/icon-kobila.png" alt="Ponvni ogled naše Kobile ;)" title="Ponvni ogled naše Kobile ;)" /></a><a class="icon" href="/siware/"><img src="<?php echo $this->getThemePath()?>/img/icon-home.png" alt="Domov" title="Domov" /></a>
				    <?php  
						$stack = Stack::getByName('main menu');
						$stack->display();
				    ?>
				</nav>
				<div class="followus">
					<p class="napis">&nbsp;Sledite nam</p>
					<p><a href="https://www.facebook.com/pages/Izdelava-spletnih-strani-SiWare/491672940897234" title="Si.Ware na FaceBook-u" class="socialIcon fb"></a><a href="https://twitter.com/SiWare_net" title="Si.Ware na Twitter-ju" class="socialIcon tw"></a><!--<a href="" class="socialIcon go"></a>--></p>
				</div>
				<div class="breadcrumbContainer cf">
					<?php
					$as = new Area('Breadcrumb');
					$as->display($c);
					?>
				</div>
			</div>
		</header>