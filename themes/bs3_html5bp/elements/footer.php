		<footer class="row">
			<div id="footer-area" class="span16">
				<div class="inner">
					<div class="copyright">&copy; 2013  Vse pravice pridržane.&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Kript, d.o.o.&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Videm 51&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;1262 Dol pri Ljubljani&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<a href="mailto:info.www@siware.net">info.www@siware.net</a></div>
				</div>
				<?php
				$ah = new Area('Footer');
				$ah -> display($c);
				?>	
			</div>
		</footer>
	</div> <!-- end of .c5wrapper -->

	<!-- JavaScript at the bottom for fast page loading -->
	<?php   Loader::element('footer_required'); ?>
	
	<!-- scripts concatenated and minified via ant build script-->
	<script src="<?php echo $this->getThemePath()?>/js/vendor/hoverIntent.js"></script>
	<script src="<?php echo $this->getThemePath()?>/js/vendor/superfish.js"></script>
	<script src="<?php echo $this->getThemePath()?>/js/plugins.js"></script>
	<script src="<?php echo $this->getThemePath()?>/js/script.js"></script>
	<!-- end scripts-->
	
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
  
</body>
</html>