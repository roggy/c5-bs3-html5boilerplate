<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

	    <div id="central" role="main" class="row cf">
			<div id="body" class="span9 pull-right">
				<?php
				$a = new Area('Main');
				$a->display($c);
				?>
			</div> <!-- end of #body -->

	        <aside id="sidebar" class="span3">
				<?php
				$as = new Area('Sidebar');
				$as->display($c);
				?>
			</aside> <!-- end of #sidebar -->
	    </div>

<?php $this->inc('elements/footer.php'); ?>