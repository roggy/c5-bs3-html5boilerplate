<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

	    <div id="central" role="main">
	    	<div class="row cf">
				<div id="body" class="span10">
					<?php
					$as = new Area('Main');
					$as->display($c);
					?>
				</div> <!-- end of #body -->
	
		        <aside id="sidebar" class="span5 offset1">
					<?php
					$as = new Area('Sidebar');
					$as->display($c);
					?>
				</aside> <!-- end of #sidebar -->
			</div>
			<div id="afterMain">
				<?php
				$as = new Area('After Main');
				$as->display($c);
				?>
			</div>
	    </div>

<?php $this->inc('elements/footer.php'); ?>